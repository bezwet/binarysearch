package test;

import main.BinarySearch;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BinarySearchTests {


    @ParameterizedTest(name = "Array consists of specified numbers {0}. Number {1} is not found in given array.")
    @MethodSource("binarySearchArgumentsForNotFound")
    public void search_numberNotFound(int[] numbers, int numberToFind) {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(numbers, numberToFind);

        assertTrue(result < 0);

    }

    @ParameterizedTest(name = "Array consists of specified numbers {0}. Number {1} is found in given array.")
    @MethodSource("binarySearchArgumentsForFoundNumber")
    public void search_numberFound(int[] numbers, int numberToFind) {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(numbers, numberToFind);

        assertTrue(result >= 0);
    }

    public static Stream<Arguments> binarySearchArgumentsForFoundNumber() {
        return Stream.of(
                Arguments.of(new int[]{13, 140, 65, 456}, 13),
                Arguments.of(new int[]{2, 1}, 2),
                Arguments.of(new int[]{1, 2}, 1),
                Arguments.of(new int[]{10, 4, 6, 25, 50, 55}, 55));
    }

    public static Stream<Arguments> binarySearchArgumentsForNotFound() {
        return Stream.of(
                Arguments.of(new int[]{13, 140, 65, 456}, 5),
                Arguments.of(new int[]{2, 1}, 5),
                Arguments.of(new int[]{1, 2}, 5),
                Arguments.of(new int[]{10, 4, 6, 25, 50, 55}, 5));
    }
}
