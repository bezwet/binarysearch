package test;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static main.Utils.getArrayOfRandomNumbers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UtilsTests {

    @ParameterizedTest(name = "Array is size of {2}. Each number is higher or equal {0} and less or equal {1}")
    @CsvSource({"1,10,5", "10,100,50", "7,1000,3"})
    public void getArrayOfRandomNumbers_test(int min, int max, int size) {
        int[] array = getArrayOfRandomNumbers(size, min, max);

        assertEquals(size, array.length);

        for (int number : array) {
            assertTrue(number <= max);
            assertTrue(number >= min);
        }

    }
}
