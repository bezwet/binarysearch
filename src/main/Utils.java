package main;

import java.util.Random;

public class Utils {
    static Random random = new Random();

    public static int[] getArrayOfRandomNumbers(int arraySize, int min, int max) {
        int[] array = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            array[i] = random.nextInt(max - min) + min;
        }
        return array;
    }
}
