package main;

public class BinarySearch {
    public int search(int[] numbers, int numberToFind) {
        int lowIndex = 0;
        int highIndex = numbers.length - 1;

        while (lowIndex <= highIndex) {
            int middlePosition = (lowIndex + highIndex) / 2;
            int middleNumber = numbers[middlePosition];

            if (numberToFind == middleNumber) {
                return middlePosition;
            }
            if (numberToFind < middleNumber) {
                highIndex = middlePosition - 1;
            } else {
                lowIndex = middlePosition + 1;
            }
        }
        return -1;
    }
}
